package services

import (
	"encoding/json"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/nats-io/nats.go"
	"github.com/nats-io/stan.go"
)

type Nats2CloudEvent struct {
}

func (Nats2CloudEvent) Convert(msg *nats.Msg) cloudevents.Event {

	event := cloudevents.NewEvent()
	_ = json.Unmarshal(msg.Data, &event)
	return event
}

func (Nats2CloudEvent) ConvertStan(msg *stan.Msg) cloudevents.Event {

	event := cloudevents.NewEvent()
	_ = json.Unmarshal(msg.Data, &event)
	return event
}
