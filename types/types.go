package types

// This file contains types which are used across the entire microservice, which may not be more appropriate elsewhere

// MyDomainData is the primary data type when receiving and sending data externally, and possibly destined to a persisent store (if applicable)
// As a implementor you should change this struct to meet the needs (e.g. a user ms would have username, first name, last name, email, etc)
type MyDomainData struct {
	MyDataField1 string
	MyDataField2 int
	MyDataField3 float64
}

type QueryReply interface {
	Reply(MyDomainData) error
}

// MyDomainDataQuery is a bundle for domain data and a query operation on that data.
// this is useful to process query operations
type MyDomainDataQuery struct {
	MyDomainData
	Op    QueryOp
	Reply QueryReply // created by adapter(or other caller) and will receive the response
}

// MyDomainDataEvent is a bundle for domain data and a query operation on that data.
// this is useful to process query operations
type MyDomainDataEvent struct {
	MyDomainData
	Event EventType
}

// QueryOp is a type to represent query operations
type QueryOp string

// EventType is a type to represent event operations
type EventType string
