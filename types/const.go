package types

import "time"

// TODO: DEVELOPER should changes these e.g. instead of "mythical*", change to "user*" for user microservice, "credentials*", "workspaces*", etc
const (
	DefaultNatsURL            = "nats://nats:4222"
	DefaultNatsMaxReconnect   = 6                // max times to reconnect within nats.connect()
	DefaultNatsReconnectWait  = 10 * time.Second // seconds to wait within nats.connect()
	DefaultNatsSleepReconnect = 60 * time.Second // seconds to sleep before retrying nats.connect()

	DefaultNatsClientID      = "mythical"
	DefaultNatsQGroup        = "mythical_qgroup"
	DefaultNatsClusterID     = "cacao-cluster"
	DefaultNatsDurableName   = "mythical_durable"
	DefaultNatsEventsSubject = "cyverse.events"
)

// These constants are for query operations to be used within the microservice
// To use these effectively, make them exactly the same as the CACAO subjects and then
// they can be reused for both the appropriate Domain and the Adapters
// TODO: TEMPLATE, we should consolidate these query "ops" in a cacao common submodule
const (
	//MythicalCatchallQueryOp QueryOp = "cyverse.mythical.*" // some microservices may want to catchall vs specific ops
	MythicalGetQueryOp  QueryOp = "cyverse.mythical.get"
	MythicalListQueryOp QueryOp = "cyverse.mythical.list"
)

// These constants are for events to be used within the microservice
// To use these affective, make them exactly the same as CACAO events and then
// they can be reused for both the appropriate Domain and Adapters
// TODO: TEMPLATE, we should consider consolidating these events in a cacao common submodule
const (
	//MythicalCatchallEvent EventType = "Mythical*" // some microservices may want a catchall vs specific events
	MythicalAddRequestedEvent    EventType = "MythicalAddRequested"
	MythicalAddedEvent           EventType = "MythicalAdded"
	MythicalDeleteRequestedEvent EventType = "MythicalDeleteRequested"
	MythicalDeletedEvent         EventType = "MythicalDeleted"
	MythicalUpdateRequestedEvent EventType = "MythicalUpdateRequested"
	MythicalUpdatedEvent         EventType = "MythicalUpdated"
)

// These constants are for channels
const (
	DefaultChannelBufferSize = 100
)
