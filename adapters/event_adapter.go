package adapters

import (
	"encoding/json"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/nats-io/stan.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-microservice-template/types"
	"gitlab.com/cyverse/cacao-microservice-template/utils"
)

// EventAdapter implements ports.IncomingEventPort
type EventAdapter struct {
	eventChan chan types.MyDomainDataEvent
	clientID  string
	subject   string
	queue     string
	sc        stan.Conn
	sub       stan.Subscription
}

// Init Initialize the adapter.
// Establish the STAN connection, obtain STAN related config from utils.Config
func (adapter *EventAdapter) Init(conf utils.Config) {

	err := adapter.connect(conf)
	if err != nil {
		log.Fatal(err)
	}

	adapter.subject = conf.NatsEventsSubject
	adapter.queue = conf.NatsQgroup
	adapter.clientID = conf.NatsClientID
}

// establish STAN connection
func (adapter *EventAdapter) connect(conf utils.Config) error {
	sc, err := stan.Connect(
		conf.NatsClusterID,
		conf.NatsClientID,
		stan.NatsURL(conf.NatsURL),
		stan.ConnectWait(5*time.Second),
	)
	if err != nil {
		return err
	}

	adapter.sc = sc
	return nil
}

// Start starts the adapter.
// Starts the subscriber.
func (adapter *EventAdapter) Start() {

	err := adapter.queueSubscribe()
	if err != nil {
		log.Fatal(err)
	}
}

func (adapter *EventAdapter) queueSubscribe() error {
	subOptions := getStanOptions(adapter.subject)
	sub, err := adapter.sc.QueueSubscribe(adapter.subject, adapter.queue, adapter.stanCallback, subOptions...)
	if err != nil {
		return err
	}
	adapter.sub = sub
	return nil
}

// InitChannel takes in the data channel for depositing incoming message
func (adapter *EventAdapter) InitChannel(data chan types.MyDomainDataEvent) {
	adapter.eventChan = data
}

func (adapter EventAdapter) stanCallback(msg *stan.Msg) {
	msg.Ack()

	event, err := GetRequestFromCloudEvent(msg.Data)
	if err != nil {
		log.WithFields(log.Fields{"error": err}).Error("unable to get request from CloudEvent")
		return
	}
	log.WithFields(log.Fields{"request": string(event.Data())}).Trace("received message")

	eventType := (types.EventType)(event.Type())
	switch eventType {
	case types.MythicalAddRequestedEvent:
		var req types.MyDomainDataEvent
		err := adapter.parseCloudEvent(event, &req.MyDomainData)
		if err != nil {
			log.Error(err)
			return
		}
		adapter.eventChan <- req
	case types.MythicalDeleteRequestedEvent:
		var req types.MyDomainDataEvent
		err := adapter.parseCloudEvent(event, &req.MyDomainData)
		if err != nil {
			log.Error(err)
			return
		}
		adapter.eventChan <- req

	default:
	}
}

func (adapter EventAdapter) parseCloudEvent(event *cloudevents.Event, data *types.MyDomainData) error {
	err := json.Unmarshal(event.Data(), &data)
	if err != nil {
		log.Error(err)
		return err
	}
	return nil
}

// Send implements ports.OutgoingQueryPort
func (adapter EventAdapter) Send(event types.MyDomainDataEvent) error {
	ce, err := CreateCloudEvent(event, string(event.Event), adapter.clientID)
	if err != nil {
		return nil
	}
	bytes, err := ce.MarshalJSON()
	if err != nil {
		return nil
	}
	adapter.sc.Publish(string(event.Event), bytes)
	return nil
}

func getStanOptions(subject string) []stan.SubscriptionOption {
	aw, _ := time.ParseDuration("60s")
	return []stan.SubscriptionOption{
		stan.DurableName("Durable" + subject),
		stan.MaxInflight(25),
		stan.SetManualAckMode(),
		stan.AckWait(aw),
	}
}
