package adapters

import (
	"encoding/json"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/rs/xid"
)

// CreateCloudEvent takes a JSON byte slice and uses it as the data for a new
// CloudEvent object Marshaled as a JSON byte slice
func CreateCloudEvent(request interface{}, eventType, source string) (*cloudevents.Event, error) {
	event := cloudevents.NewEvent()
	id := xid.New()
	event.SetID(id.String())
	event.SetType(eventType)
	event.SetTime(time.Now())
	event.SetSource(source)
	err := event.SetData(cloudevents.ApplicationJSON, request)
	if err != nil {
		return nil, err
	}

	return &event, nil
}

// GetRequestFromCloudEvent ...
func GetRequestFromCloudEvent(msgData []byte) (*cloudevents.Event, error) {
	event := cloudevents.NewEvent()
	err := json.Unmarshal(msgData, &event)
	if err != nil {
		return nil, err
	}

	return &event, nil
}
