package adapters

import (
	"encoding/json"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/nats-io/nats.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-microservice-template/types"
	"gitlab.com/cyverse/cacao-microservice-template/utils"
)

// QueryAdapter implements ports.IncomingQueryPort & ports.OutgoingQueryPort
type QueryAdapter struct {
	queryChan chan types.MyDomainDataQuery
	clientID  string
	subject   string
	queue     string
	nc        *nats.Conn
	sub       *nats.Subscription
}

// NewQueryAdapter creates a new QueryAdapter
func NewQueryAdapter(subject string) *QueryAdapter {
	return &QueryAdapter{subject: subject}
}

// Init perform initialization on the adapter
func (adapter *QueryAdapter) Init(conf utils.Config) {
	err := adapter.natsConnect(conf)
	if err != nil {
		log.Fatal(err)
	}

	adapter.clientID = conf.NatsClientID
	adapter.queue = conf.NatsQgroup
}

// natsConnect establish the NATS connection
func (adapter *QueryAdapter) natsConnect(conf utils.Config) error {
	nc, err := nats.Connect(conf.NatsURL)
	if err != nil {
		return err
	}
	log.Info("NATS connection established")

	adapter.nc = nc

	return nil
}

// InitChannel takes in the data channel for depositing incoming message
func (adapter *QueryAdapter) InitChannel(c chan types.MyDomainDataQuery) {
	adapter.queryChan = c
}

// Start starts the queue subscriber
func (adapter *QueryAdapter) Start() {
	err := adapter.natsQueueSubscribe()
	if err != nil {
		log.Fatal(err)
	}
}

// natsQueueSubscribe starts the subscriber
func (adapter *QueryAdapter) natsQueueSubscribe() error {
	// subscribe with adapter.natsCallbck()
	sub, err := adapter.nc.QueueSubscribe(adapter.subject, adapter.queue, adapter.natsCallback)
	if err != nil {
		return err
	}
	adapter.sub = sub
	return nil
}

func (adapter QueryAdapter) natsCallback(m *nats.Msg) {

	ce, err := GetRequestFromCloudEvent(m.Data)
	if err != nil {
		log.WithFields(log.Fields{"error": err}).Error("unable to get request from CloudEvent")
		return
	}
	log.WithFields(log.Fields{"request": string(ce.Data())}).Trace("received message")

	queryOp := (types.QueryOp)(ce.Type())
	switch queryOp {
	case types.MythicalGetQueryOp:
		var query types.MyDomainDataQuery
		err = adapter.parseCloudEvent(ce, &query.MyDomainData)
		if err != nil {
			log.Error(err)
		}

		// create reply object to be passed to domain
		query.Reply = QueryReply{adapter: &adapter, msg: m}

		// deposit query into channel
		adapter.queryChan <- query
	case types.MythicalListQueryOp:
		var query types.MyDomainDataQuery
		err = adapter.parseCloudEvent(ce, &query.MyDomainData)
		if err != nil {
			log.Error(err)
		}

		// create reply object to be passed to domain
		query.Reply = QueryReply{adapter: &adapter, msg: m}

		// deposit query into channel
		adapter.queryChan <- query
	default:
	}

}

func (adapter QueryAdapter) parseCloudEvent(event *cloudevents.Event, data *types.MyDomainData) error {
	err := json.Unmarshal(event.Data(), &data)
	if err != nil {
		log.Error(err)
		return err
	}
	return nil
}

// Send implements ports.OutgoingQueryPort
func (adapter QueryAdapter) Send(queryOp types.QueryOp, data interface{}) error {
	ce, err := CreateCloudEvent(data, string(queryOp), adapter.clientID)
	if err != nil {
		return nil
	}
	bytes, err := ce.MarshalJSON()
	if err != nil {
		return nil
	}
	adapter.nc.Publish(string(queryOp), bytes)
	return nil
}

// Reply replys a NATS msg with a response
// Alternative, this could also depoisit the response to an internal go channel
func (adapter QueryAdapter) Reply(msg *nats.Msg, response types.MyDomainData) error {
	// uses reply subject as event type and uses client id as event source
	ce, err := CreateCloudEvent(response, msg.Reply, adapter.clientID)
	if err != nil {
		return err
	}
	dat, err := ce.MarshalJSON()
	if err != nil {
		return err
	}
	return msg.Respond(dat)
}

// QueryReply implements types.QueryReply
type QueryReply struct {
	msg     *nats.Msg
	adapter *QueryAdapter
}

// Reply send a response
func (r QueryReply) Reply(response types.MyDomainData) error {
	return r.adapter.Reply(r.msg, response)
}
