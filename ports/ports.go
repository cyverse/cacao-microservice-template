package ports

import (
	"gitlab.com/cyverse/cacao-microservice-template/types"
	"gitlab.com/cyverse/cacao-microservice-template/utils"
)

// Port is the base interface for Ports, in which all should support an Init() function (but it may be noop)
type Port interface {
	Init(config utils.Config)
}

// AsyncPort is a special type of port that will has an asynchronous behavior to it, such as being event-driven
// outside of the primary domain thread (e.g. incoming message queues). This ports should communicate with the
// domain object using a threaded approach (i.e. go routine, go channels and possibly waitgroups)
type AsyncPort interface {
	Port
	Start()
}

// IncomingQueryPort is an example interface for a query port.
// Internal to the adapter there may be additional methods, but at the very least, it is an async port
// Also, incoming and outgoing ports are declared separately for illustrative purposes
// method set signatures will allow one adapter fullfill the purposes of both income and outoing query ports
type IncomingQueryPort interface {
	AsyncPort
	InitChannel(data chan types.MyDomainDataQuery)
}

// IncomingEventPort is an example interface for an event port.
type IncomingEventPort interface {
	AsyncPort
	InitChannel(data chan types.MyDomainDataEvent)
}

// OutgoingQueryPort does not need to be asynchronous, but is an example outgoing port
type OutgoingQueryPort interface {
	Port
	Send(types.QueryOp, interface{}) error // the interface{} should be whatever the atucal outgoing query is
}

// OutgoingEventPort does not need to be asynchronous, but is an example outgoing event port
type OutgoingEventPort interface {
	Port
	Send(types.MyDomainDataEvent) error // the parameter should be whatever the actual outgoing event be
}

// PersistentStoragePort is example interface for an driven port for Perstistent Storage.
type PersistentStoragePort interface {
	Port
	Create(d types.MyDomainData) error
	Read(d types.MyDomainData) error
	Update(d types.MyDomainData, e types.MyDomainData) error
	Delete(d types.MyDomainData) error
	Fetch() []*types.MyDomainData
}

// TODO: TEMPLATE, OLD interfaces -- will most likely need modify or delete

/* //	Example interface for a port. Normally it would have more functions than just Create, you may have a full
//	CRUD interface here, or any additional functions for your ports.
type ReplyPort interface {
	Get(msg cloudevents.Event) cloudevents.Event
}

type EventPort interface {
	Create(msg cloudevents.Event) error
}

//	Example interface for an outgoing port.
// 	has two functions to store & get data.
type StoragePort interface {
	Store(msg cloudevents.Event) error
	Get(msg cloudevents.Event) cloudevents.Event
} */
