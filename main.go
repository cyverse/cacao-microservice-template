package main

import (
	"log"

	"github.com/kelseyhightower/envconfig"
	"gitlab.com/cyverse/cacao-microservice-template/adapters"
	"gitlab.com/cyverse/cacao-microservice-template/domain"
	"gitlab.com/cyverse/cacao-microservice-template/utils"
)

/*
This is a template for the base microservice that uses NATS streaming and NATS core.

This example is based on two incoming adapters and one outgoing adapter to mock some
sort of storage system.


*/

func main() {

	var config utils.Config
	err := envconfig.Process("", &config)
	if err != nil {
		log.Fatal(err.Error())
	}
	config.ProcessDefaults()

	// create an initial Domain object
	var dmain domain.Domain

	// add and initialize the storage adapter
	dmain.DataStore = &adapters.MongoAdapter{}
	dmain.DataStore.Init(config)

	// add and initialize the query adapter
	dmain.QueryOut = adapters.QueryAdapter{}
	dmain.QueryIn = adapters.Query
	dmain.QueryOut.Init(config)

	// add and initialize the events adapter
	dmain.EventsIn = adapters.EventsAdapter{}
	dmain.EventsOut = dmain.EventsIn
	dmain.EventsIn.Init(config)

	dmain.Init(config)
	dmain.Start()
}
