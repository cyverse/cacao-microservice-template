module gitlab.com/cyverse/cacao-microservice-template

go 1.14

require (
	github.com/cloudevents/sdk-go/v2 v2.3.1
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/nats-io/nats-streaming-server v0.20.0 // indirect
	github.com/nats-io/nats.go v1.10.0
	github.com/nats-io/stan.go v0.8.2
	github.com/rs/xid v1.2.1
	github.com/sirupsen/logrus v1.7.0
	go.mongodb.org/mongo-driver v1.4.6
	google.golang.org/protobuf v1.25.0 // indirect
)
