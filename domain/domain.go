package domain

import (
	"log"
	"sync"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-microservice-template/ports"
	"gitlab.com/cyverse/cacao-microservice-template/types"
	"gitlab.com/cyverse/cacao-microservice-template/utils"
)

// Domain is the base struct for the domain service
// TODO: Developer should remove any ports that are not being used
type Domain struct {
	DataStore ports.PersistentStoragePort
	QueryIn   ports.IncomingQueryPort
	QueryOut  ports.OutgoingQueryPort
	EventsIn  ports.IncomingEventPort
	EventsOut ports.OutgoingEventPort
}

// HandlerFunc is a general interface that defines all Handlers
type HandlerFunc interface {
	handle(*types.MyDomainData)
}
type QueryHandleFunc HandlerFunc
type EventHandleFunc HandlerFunc

// Init initializes all the specified adapters
// TODO: DEVELOPER should remove ports not being used
func (d *Domain) Init(c utils.Config) {
	log.Println("starting domain.Init()")

	// TODO: TEMPLATE, insert handlers here
	// Examples
	// d.addQueryHandleFunc (util.types.MYTHICAL_CATCHALL_QUERYOP, CatchAllQueryHandler)
	// d.addEventHandleFunc (util.types.MYTHICAL_CATCHALL_EVENT, CatchAllEventHandler)
}

// Start will start the domain object, and in turn start all the async adapters
func (d *Domain) Start() {
	log.Println("starting domain.Start()")

	// using waitgroups to block termination gracefully
	var wg sync.WaitGroup

	// It is here that we need to handle asynchronous adapters
	// create channel for incoming queries, qchan
	qchannel := make(chan types.MyDomainDataQuery, types.DefaultChannelBufferSize)
	d.QueryIn.InitChannel(qchannel)
	go d.QueryIn.Start()

	// start the domain's query worker
	wg.Add(1)
	go d.processQueryWorker(qchannel, wg)

	echannel := make(chan types.MyDomainDataEvent, types.DefaultChannelBufferSize)
	d.EventsIn.InitChannel(echannel)
	go d.EventsIn.Start()

	// start the domain's query worker
	wg.Add(1)
	go d.processEventWorker(echannel, wg)

	wg.Wait()
}

func (d *Domain) processQueryWorker(qchannel chan types.MyDomainDataQuery, wg sync.WaitGroup) {
	log.Println("starting domain.processQueryWorker()")
	defer wg.Done()

	for dataQueryOp := range qchannel {
		// TODO: TEMPLATE add a better log line
		log.Println("domain.processQueryWorker received data")

		// TODO: TEMPLATE add code to execute the handler appropriate for the query op
		log.Print(dataQueryOp)
	}
}

func (d *Domain) processEventWorker(echannel chan types.MyDomainDataEvent, wg sync.WaitGroup) {
	log.Println("starting domain.processEventWorker()")
	defer wg.Done()

	for dataEvent := range echannel {
		// TODO: TEMPLATE add a better log line
		log.Println("domain.processEventWorker received data")

		// TODO: TEMPLATE add code to execute the handler appropriate for the event
		log.Print(dataEvent)
	}
}

// addQueryHandler needs implementation
func (d *Domain) addQueryHandlerFunc(q types.MyDomainDataQuery, h *QueryHandleFunc) {

}

// addEventHandler needs implementation
func (d *Domain) addEventHandlerFunc(e types.MyDomainDataEvent, h *EventHandleFunc) {

}

// OLDER METHODS -- may need to refactored or deleted

// Create is the actual core logic of the process, here the program takes cloud events and does whatever with them,
// processing them and passing them onto whatever port/adapter they need to goto.
func (m Domain) Create(msg cloudevents.Event) error {
	_ = m.storeVitalInformation(msg)
	return nil
}

func (m Domain) Get(msg cloudevents.Event) cloudevents.Event {
	return m.getVitalInformation(msg)
}

func (m Domain) storeVitalInformation(msg cloudevents.Event) error {
	// return m.DataStore.Create(TODO)
	panic("TODO")
}

func (m Domain) getVitalInformation(msg cloudevents.Event) cloudevents.Event {
	// return m.DataStore.Read(TODO)
	panic("TODO")
}
