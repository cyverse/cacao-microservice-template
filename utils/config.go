package utils

import (
	"log"

	"gitlab.com/cyverse/cacao-microservice-template/types"
)

// Config is the configuration settings, which can be used by the Domain object or Adapters
type Config struct {

	// The following fields are used by both nats query channel and nats streaming
	NatsURL      string `envconfig:"NATS_URL" default:"nats://nats:4222`
	NatsClientID string `envconfig:"NATS_CLIENT_ID"`
	NatsQgroup   string `envconfig:"NATS_QGROUP"`

	// The following fields are used only by the nats query channel
	NatsClusterID     string `envconfig:"NATS_CLUSTER_ID" default:"cacao-cluster"`
	NatsDurableName   string `envconfig:"NATS_DURABLE_NAME"`
	NatsEventsSubject string `envconfig:"NATS_EVENTS_SUBJECT" default:"cyverse.events"`
}

// ProcessDefaults will take a Config object and process the config object further, including
// populating any null values
// TODO: TEMPLATE: should figure out a consistent way of generating a unique but repeatable nats client id for the case when
// a microservice is part of a ReplcaSet. It might be the case that the same client doesn't need the same client id
// as long as the clients are part of the same qgroup and durable name
func (c *Config) ProcessDefaults() {
	log.Println("starting config.ProcessConfig()")
	if c.NatsURL == "" {
		c.NatsURL = types.DefaultNatsURL
	}
	if c.NatsClientID == "" {
		c.NatsClientID = types.DefaultNatsClientID
	}
	if c.NatsQgroup == "" {
		c.NatsQgroup = types.DefaultNatsQGroup
	}
	if c.NatsClusterID == "" {
		c.NatsClusterID = types.DefaultNatsClusterID
	}
	if c.NatsDurableName == "" {
		c.NatsDurableName = types.DefaultNatsDurableName
	}
	if c.NatsEventsSubject == "" {
		c.NatsEventsSubject = types.DefaultNatsEventsSubject
	}
}
